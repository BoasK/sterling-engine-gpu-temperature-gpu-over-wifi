const bool debug = true; //decides if mesages are writen to the console
const bool Verbose = true; //decides if mesages are writen to the console

const size_t CAPACITY = JSON_ARRAY_SIZE(9) + JSON_OBJECT_SIZE(1) + 9 * JSON_OBJECT_SIZE(4) + JSON_OBJECT_SIZE(7) + 620; //defines the amount of ram alocated to the json deserialize, to calculate how big this needs to be go to: https://arduinojson.org/v5/assistant/. 

int ThermistorPin = 0; //the pin used to read the temperature risistor.
#define heaterPin D8 //the pin used to open and close the Relay, HIGH is open and LOW is closed.
int minTemp = 30; //the minimum temperatture needed to spin the engine, this ensures it is always rotating.

const long jsonPullTime = 5;//the time in seconds between pulling the json file.
const long showIntervalTime = 10;//the time in seconds a entry is on the screen, after this time it wil go to the next entry.
const long tempIntervalTime = 5;//the time in seconds between calculating the temperature and ajusting the heater acordingly.

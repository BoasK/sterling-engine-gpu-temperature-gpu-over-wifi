#include <ArduinoJson.h>

#include "settings.h" //the settings 
#include "wifipass.h" //this file contains the ssid and the password of my wifi network

//######## ######## begin oled config
/*  connect the wires in the folowing way
    grn -> grn
    vcc -> 3v
    scl -> D1
    sda -> D2

    top yello part is 16px high, bottom blue part 48
   schreen is 128px wide
*/
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define OLED_RESET D5
/* Object named display, of the class Adafruit_SSD1306 */
Adafruit_SSD1306 display(OLED_RESET);
#if (SSD1306_LCDHEIGHT != 64)
#error("Height incorrect, please fix Adafruit_SSD1306.h!");
#endif
uint8_t s = 0, m = 0, h = 0;
//######## ######## end oled

//######## ######## begin wifi config
#include "ESP8266WiFi.h"
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
String sensorReadings;//define the string the json is loaded into.
int entryIndex = 0;//the index of the entrys that tis shown
//######## ######## end wifi config

//######## ######## start json config
String Name;
String data;
String unit;
int amountOfEntrys = 0;
//######## ######## end json config

//######## ######## start declerations for the termometer
//I copyed the calculation form: https://www.circuitbasics.com/arduino-thermistor-temperature-sensor-tutorial/
int Vo;
float R1 = 10000;
float logR2, R2, Temperature;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;
int gpuTemp; //the real world temprature of the serling engine
bool heating; //saves if the heater is on

/* the termonitor is connected the folowing way
   3v -> NTC
   NTC -> a0 -> 10k resistor
   10 resistor -> grn
*/
//######## ######## end declerations for the termometer

//######## ######## start declerations interupts
//source https://www.youtube.com/watch?v=D_7ciW_TCac
#include <Ticker.h>
Ticker timer;
int amountOfInterrupts = 0;
bool pullJson = false;
bool nextShow = false;
bool tempInterval = false;
//######## ######## end declerations interupts

void setup()   {

  pinMode(heaterPin, OUTPUT);
  digitalWrite(heaterPin, HIGH);

  //serial
  if (debug) {
    Serial.begin(74880);
  }

  //oled
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C); /* Initialize display with address 0x3C */
  display.setTextColor(WHITE);  /* Color of text*/
  display.setTextWrap(false);
  display.setTextSize(2);
  display.clearDisplay();  /* Clear display */
  drawStr(10, 20, "opstarten");
  display.display();

  //connect to wifi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.print("connected to WIFI, ip: ");
  Serial.println(WiFi.localIP());

  getJson();//get the first json and deserialize
  writeOLED(); //write first sting to the oled

  timer.attach(1, ISR);
}

void ISR() {
  if (Verbose) {
    Serial.println("interrupt called");
  }
  amountOfInterrupts++;
  if (amountOfInterrupts % jsonPullTime == 0) {
    pullJson = true;
  }
  if (amountOfInterrupts % showIntervalTime == 0) {
    nextShow = true;
  }
  if (amountOfInterrupts % tempIntervalTime == 0) {
    tempInterval = true;
  }
}

void loop() {
  if (nextShow) {
    nextShow = false;
    if (entryIndex >= amountOfEntrys) { //if the index is grater then the amount of entrys reset to 0
      entryIndex = 0;
    } else {
      entryIndex++;
    }
    writeOLED();
  }
  if (pullJson) {
    pullJson = false;
    getJson();
    writeOLED();
  }
  if (tempInterval) {
    tempInterval == false;
    temp();
    heater();
  }

}

void getJson() {
  //initilize the required json classes
  DynamicJsonDocument jsonBuffer(CAPACITY);
  DeserializationError err;

  //get the json and deserilize it.
  sensorReadings = httpGETRequest(serverName);
  if (Verbose) {
    Serial.println("Sensor readings in String");
    Serial.println(sensorReadings);
  }
  delay(2);
  err = deserializeJson(jsonBuffer, sensorReadings);
  Serial.print("deserializeJson() ran with code: \"");
  Serial.print(err.c_str());
  Serial.println("\"");

  if (err.c_str() == "Ok") { // only fill the local variables if the data transfer was sucesfull
    String record = jsonBuffer["afterburner"]["entries"][entryIndex];//write the current selected entry to the console.
    Serial.print("entry that is beeing shown: ");
    Serial.println(record);
    Name = jsonBuffer["afterburner"]["entries"][entryIndex]["name"].as<String>();//save the relavent data points as strings.
    data = jsonBuffer["afterburner"]["entries"][entryIndex]["data"].as<String>();
    unit = jsonBuffer["afterburner"]["entries"][entryIndex]["unit"].as<String>();
    amountOfEntrys = jsonBuffer["afterburner"]["entries"].size();//returns the amount of entrys recived from the json
    gpuTemp = jsonBuffer["afterburner"]["entries"][4]["data"];//get the temperature of the gpu.
    Serial.printf("gpuTemp: %d\n", gpuTemp);
    jsonBuffer.clear();//free up ram space.
    
  }
}


void writeOLED() {

  if (Name == "null") {//if thare is no coneection display error message.
    display.clearDisplay();  /* Clear display */

    display.setTextSize(2);
    drawStr(40, 17, "niet");
    drawStr(10, 37, "verbonden");
    display.display();
  } else {
    Name.replace("GPU1", "GPU");//just a small preference thing.
    unit.replace("°C", "'C"); //° is not supported on sutch a small scale you will not notice the diffrence.
    char nameChar[Name.length() + 1];//the schreen prints a char array, these are inisilized here.
    char dataChar[data.length() + 1];
    char unitChar[unit.length() + 1];
    Name.toCharArray(nameChar, Name.length() + 1);//converts the string to a char array and save in the newly initilized array.
    data.toCharArray(dataChar, data.length() + 1);
    unit.toCharArray(unitChar, unit.length() + 1);

    int nameLocation = (128 - (6 * Name.length())) / 2; //total with minus the with of the text, the letters are 5 px wide with a blank row at the rigt side, devide by 2 to get destance to sides.
    int dataLocation = ((128 - (3 * 6 * (data.length() + unit.length()))) / 2); //total with minus the with of the float, unit and one space, the letters are 5 px wide in font 3 so 3*5=15, devide by 2 to get destance to sides.
    int unitLocation = dataLocation + (3 * 6 * (data.length())); //same offset as the data location only with the data length added, 5 characters plus a wite space, times 6 px times font size 3 so 3*6(5+1) = 108
    dataLocation = dataLocation < 0 ? 0 : dataLocation;

    Serial.printf("writing to the schreen\n");//write debug info to the console
    Serial.printf("location of the entry's name: %d\n", nameLocation);
    Serial.printf("the name of the entry: %s\n", nameChar);
    Serial.printf("amount of characters in the name: %d\n", sizeof(nameChar));
    Serial.printf("location of the entry's data: %d\n", dataLocation);
    Serial.printf("the entry's data: %s\n", dataChar);
    Serial.printf("the entry's unit: %s\n", unitChar);

    //writing the data to the schreen.
    display.clearDisplay();  /* Clear display */
    display.setTextSize(1);
    drawStr(nameLocation, 4, nameChar);
    display.setTextSize(3);
    drawStr(dataLocation, 20, dataChar);
    drawStr(unitLocation, 20, unitChar);
    display.display();
    Serial.println("done writing on schreen");
  }
}

//this was in the oled library
void drawStr(uint8_t x, uint8_t y, char* str) {
  display.setCursor(x, y);  /* Set x,y coordinates */
  display.println(str);
}

//tnx to https://randomnerdtutorials.com/esp8266-nodemcu-http-get-post-arduino/#http-get-2
String httpGETRequest(const char* serverName) {
  HTTPClient http;

  // Your IP address with path or Domain name with URL path
  http.begin(serverName);

  // Send HTTP POST request
  int httpResponseCode = http.GET();

  String payload = "{}";

  //writte debug info to the console
  if (httpResponseCode > 0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}

void temp() { //I copyed the calculation form: https://www.circuitbasics.com/arduino-thermistor-temperature-sensor-tutorial/

  digitalWrite(heaterPin, HIGH);// tun off the heater to improve reading
  delay(5);
  Vo = analogRead(ThermistorPin);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  Temperature = (1.0 / (c1 + c2 * logR2 + c3 * logR2 * logR2 * logR2)); //calculates the temperature in kelvin
  Temperature = Temperature - 273.15; //the temprature is converted the c

  Serial.printf("real world temperature: %2.2f c\n", Temperature);
}

void heater() {
  if (gpuTemp < minTemp || Name == "null") { //makes sure the engine is always hot enough to turn, if no data is avalible is alow serves as a default value.
    if (heating) {
      if (Temperature > minTemp) {
        heating = false;
      }
    } else {
      if (Temperature < minTemp) {
        heating = true;
      }
    }
  }
  else {
    if (heating) {
      if (Temperature > gpuTemp) {
        heating = false;
      }
    } else {
      if (Temperature < gpuTemp) {
        heating = true;
      }
    }
  }
  if (heating) {
    digitalWrite(heaterPin, LOW);
  }
  else {
    digitalWrite(heaterPin, HIGH);
  }
}
